package com.example.otweet.app.activities;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.otweet.app.R;
import com.example.otweet.app.authorization.AuthorizationActivity;
import com.example.otweet.app.domain.OTweetApplication;
import com.example.otweet.app.domain.StatusListAdapter;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;


public class StatusListActivity extends ListActivity {


    private OTweetApplication app;
    private Twitter twitter;
    private ProgressDialog progressDialog;
    private LoadMoreListItem headerView;
    private LoadMoreListItem footerView;
    private StatusListAdapter adapter;

    //flags to keep track of the async task
    //the only reason we are doing is this is because I want to use a singly TwitterAsyncTask

    private final int GET_CLEAN = 0;
    private final int GET_NEWER= 1;
    private final int GET_OLDER = 2;
    private int lastOperation; //0 for new timeline, 1 for newer items, 2 for older items.
    private long firstId;
    private long lastId;

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id){
        if (v.equals(headerView)){
            headerView.showProgress();
            loadNewerTweets();
        }else if(v.equals(footerView)){
            footerView.showProgress();
            loadOlderTweets();
        }
    }

    private void loadTimelineIfNotLoaded() {
        if (getListAdapter() == null){
            loadHomeTimeline();
        }
    }

    private void loadHomeTimeline() {
        try{
            new TwitterAsyncTask().execute(lastOperation = GET_CLEAN);
        }catch (Exception e){
            Log.e(OTweetApplication.APP_TAG,e.getMessage());
        }
    }

    private void toggleProgressDialog(){
        progressDialog.setMessage(getResources().getString(R.string.wait_loading_feed));
        if (progressDialog.isShowing()) progressDialog.dismiss();
        else progressDialog.show();
    }

    private void loadNewerTweets() {
        headerView.hideProgress();
        new TwitterAsyncTask().execute(lastOperation = GET_NEWER,firstId = adapter.getFirstId());
    }

    private void loadOlderTweets(){
        footerView.hideProgress();
        new TwitterAsyncTask().execute(lastOperation = GET_OLDER, lastId = adapter.getLastId());
    }

    private void updateAdapter(ResponseList list){
        if (list!=null && lastOperation==0){
            adapter = new StatusListAdapter(this,list);
            setLoadMoreView();
            setListAdapter(adapter);
            getListView().setSelection(1);
        }else if (list!=null && lastOperation==1){
            adapter.appendNewer(list);
            getListView().setSelection(1);
        }else if (list!=null && lastOperation==2){
            adapter.appendOlder(list);
            getListView().setSelection(adapter.getCount()-1);
        }
        else{
            failWithCode(-1);
        }

    }

    private void setLoadMoreView() {
        headerView = (LoadMoreListItem) getLayoutInflater().inflate(R.layout.load_more, null);
        footerView = (LoadMoreListItem) getLayoutInflater().inflate(R.layout.load_more, null); //same, just change the text
        headerView.showHeaderText();
        footerView.showFooterText();
        getListView().addHeaderView(headerView);
        getListView().addFooterView(footerView);
    }

    /***
     * Displays a failure message.
     * @param reason The failure reason. -1 for failured to get twitter feed, for whatever reason.
     */
    private void failWithCode(int reason) {
        switch (reason){
            case -1:
                Crouton.showText(this,getResources().getText(R.string.message_failed_feed), Style.ALERT);
        }
    }


    //<editor-fold desc="Activity Lifecycle">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (OTweetApplication)getApplication();
        twitter = app.getTwitter();
        setContentView(R.layout.activity_status_list);
        progressDialog = new ProgressDialog(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (!app.isAuthorized()){
            beginAuthorization();
        }else{
            loadTimelineIfNotLoaded();
        }
    }
    //</editor-fold>


    private void beginAuthorization(){
        Intent intent = new Intent(this, AuthorizationActivity.class);
        startActivity(intent);
    }


    private class TwitterAsyncTask extends AsyncTask<Object,Void,Object>{

        @Override
        protected void onPreExecute(){
            toggleProgressDialog();
        }

        @Override
        protected Object doInBackground(Object... params) {

            //0 for new timeline
            //1 for newer items
            //2 for older items
            int option = (Integer)params[0];
            try {
                if (option==GET_CLEAN){
                    return twitter.getHomeTimeline();
                }else if (option==GET_NEWER){
                    return twitter.getHomeTimeline(new Paging().sinceId((Long)params[1]));
                }else if (option==GET_OLDER){
                    return twitter.getHomeTimeline(new Paging().sinceId((Long)params[1]));
                }
            } catch (TwitterException e) {
                Log.e(OTweetApplication.APP_TAG,"Unable to complete TwitterAsyncTask: " +
                e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result){
            updateAdapter((ResponseList) result);
            toggleProgressDialog();
        }
    }
    //</editor-fold>
}

