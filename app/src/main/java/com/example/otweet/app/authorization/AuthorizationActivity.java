package com.example.otweet.app.authorization;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.example.otweet.app.R;
import com.example.otweet.app.domain.AuthorizationListener;
import com.example.otweet.app.domain.OTweetApplication;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import static com.example.otweet.app.domain.OTweetApplication.isDebug;
import static com.example.otweet.app.domain.OTweetApplication.APP_TAG;

public class AuthorizationActivity extends Activity implements
        AuthorizationListener{

    private OTweetApplication app;
    private WebView webView;
    private ProgressDialog progressDialog;

    /***
     * Let's us override the webclient basic functions, like onPageFinished, etc.
     */
    private WebViewClient webViewClient = new WebViewClient(){
         @Override
        public void onLoadResource(WebView view, String url){
            Uri uri = Uri.parse(url);
            if (uri.getHost().equals("otweet.com")){
                String token = uri.getQueryParameter("oauth_token");
                String oauthVerifier = uri.getQueryParameter("oauth_verifier");
                if (token!=null && oauthVerifier!=null){
                    webView.setVisibility(View.INVISIBLE);
                    app.requestAccessToken(token, oauthVerifier); //needs to be moved to a callback
                }else{
                    failAuthorization();
                }
            }else{
                super.onLoadResource(view,url);
            }
        }
        @Override
        public void onPageFinished(WebView view, String url){
            if(isDebug()){
              Toast.makeText(getApplicationContext(),
                      url,Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss(); //here
        }
    };



    public void requestTokenReady(String url){
        //toggleProgressDialog();
        webView.loadUrl(url);
    }

    public void accessTokenReady(){
        finish();
    }

    @Override
    public void failAuthorization() {
        Crouton.showText(this,getResources().getText(R.string.message_failed_authorization), Style.ALERT);
        progressDialog.dismiss();
    }


    //<editor-fold desc="Activity Lifecycle">
    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        Log.d(APP_TAG, "hitting AuthAct onCreate()");
        app = (OTweetApplication)getApplication();
        setContentView(R.layout.authorization_view);
        setUpViews();
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(APP_TAG, "hitting AuthAct onResume()");
        app.requestRequestToken(this);
        toggleProgressDialog();
    }

    @Override
    public void finish(){
        //toggleProgressDialog();
       Log.d(APP_TAG, "hitting AuthAct finish()");
       super.finish();
    }
    //</editor-fold>

    private void setUpViews() {
        webView = (WebView)findViewById(R.id.web_view);
        webView.setWebViewClient(webViewClient);
        progressDialog = new ProgressDialog(this);
    }

    public void toggleProgressDialog(){
        progressDialog.setMessage(getResources().getString(R.string.wait_authorizing));
        if(progressDialog.isShowing()) progressDialog.dismiss();
        else progressDialog.show();
    }

}
