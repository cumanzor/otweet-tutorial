package com.example.otweet.app.domain;

/***
 * Defines the necessary callbacks to complete a Twitter OAuth authorization transaction.
 * Useful if the authorization steps are done outside of the activity that's implementing this.
 */
public interface AuthorizationListener {
        void requestTokenReady(String url);
        void accessTokenReady();
        void failAuthorization();
}
