package com.example.otweet.app.domain;


import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.example.otweet.app.BuildConfig;
import com.example.otweet.app.authorization.OAuthHelper;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class OTweetApplication extends Application{

    public static final String APP_TAG = "Otweet";

    private OAuthHelper oAuthHelper;
    private RequestToken currentRequestToken;
    private Twitter twitter;
    private AuthorizationListener authListener;

    public static Boolean isDebug(){
        return BuildConfig.DEBUG;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        oAuthHelper = new OAuthHelper(this);
        twitter = new TwitterFactory().getInstance();
        oAuthHelper.configureOAuth(twitter);
    }


    public boolean isAuthorized(){
        return oAuthHelper.hasAccessToken();
    }

    /***
     * First step in the request process, this method calls for a RequestToken request,
     * resulting in a callback URL that is required to request an AccessToken.
     * @see #requestAccessToken(String, String)
     * @param listener the object that will receive the authorization token.
     */
    public void requestRequestToken(AuthorizationListener listener) {
        this.authListener = listener;
        if (currentRequestToken == null){
            try {
                new getTokenTask().execute(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            listener.requestTokenReady(currentRequestToken.getAuthenticationURL());
        }

        //return currentRequestToken.getAuthorizationURL();
        //return null;
    }

    /***
     * This method runs after the RequestToken has been
     * validated, requests an AccessToken and saves it locally.
     * @see #requestRequestToken(AuthorizationListener)
     * @param token a string representing a token, in case we need it.
     * @param verifier the oauth_verifier, required to get the AccessToken.
     */
    public void requestAccessToken(String token, String verifier){
        try {
            new getTokenTask().execute(1, verifier);
        } catch (Exception e) {
            //TODO: unable to authorize user, display message
            Log.e(APP_TAG, "Unable to authorize user. See stack trace below..."+
            e.getMessage());
        }

    }



    private void setToken(Object token){
        if(token==null){
            Log.e(APP_TAG,"Unable to authorize.");
            authListener.failAuthorization();
        }else if (token instanceof RequestToken){
            currentRequestToken = (RequestToken)token;
            authListener.requestTokenReady(currentRequestToken.getAuthenticationURL());
        }else if (token instanceof AccessToken){
            oAuthHelper.storeAccessToken((AccessToken)token);
            authListener.accessTokenReady();
        }
    }

    private class getTokenTask extends AsyncTask<Object, Void, Object>{

//        @Override
//        protected void onPreExecute(){
//        }

        @Override
        protected Object doInBackground(Object... params) {
            //0 = RequestToken
            //1 = AccessToken
            int option = (Integer)params[0];
            try {
                if (option==0) {
                   return twitter.getOAuthRequestToken();
                }else if(option==1){
                    String oauthVerifier = (String)params[1];
                    return twitter.getOAuthAccessToken(oauthVerifier);
                }
            }catch(TwitterException e){
                Log.e(OTweetApplication.APP_TAG,"here be errors!!");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result){
            setToken(result);
        }

    }

    public Twitter getTwitter() {
        return twitter;
    }
}
