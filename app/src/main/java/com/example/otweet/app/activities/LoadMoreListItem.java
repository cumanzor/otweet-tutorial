package com.example.otweet.app.activities;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.otweet.app.R;

import org.w3c.dom.Text;

public class LoadMoreListItem extends RelativeLayout{

    private TextView loadMoreText;
    private ProgressBar progressIndicator;

    public LoadMoreListItem(Context c, AttributeSet attrs){
        super(c,attrs);
    }

    @Override
    public void onFinishInflate(){
        findViews();
    }

    public void showProgress(){
        progressIndicator.setVisibility(View.VISIBLE);
    }

    public void hideProgress(){
        progressIndicator.setVisibility(View.INVISIBLE);
    }

    public void showHeaderText(){
        String headerText = getResources().getString(R.string.load_more_header);
        loadMoreText.setText(headerText);
    }


    public void showFooterText(){
        String headerText = getResources().getString(R.string.load_more_footer);
        loadMoreText.setText(headerText);
    }


    private void findViews() {
        loadMoreText = (TextView)findViewById(R.id.load_more_text);
        progressIndicator = (ProgressBar)findViewById(R.id.load_more_progress_bar);
    }

}
