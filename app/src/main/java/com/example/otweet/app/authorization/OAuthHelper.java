package com.example.otweet.app.authorization;

import android.content.Context;
import android.content.SharedPreferences;
import com.example.otweet.app.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import twitter4j.Twitter;
import twitter4j.auth.AccessToken;

public class OAuthHelper {

    private static final String APPLICATION_PREFERENCES = "app_prefs";
    private static final String AUTH_KEY = "auth_key";
    private static final String AUTH_SECRET_KEY = "auth_secret_key";

    private SharedPreferences prefs;
    private AccessToken accessToken;
    private String consumerSecretKey;
    private String consumerKey;
    private Context context;

    public OAuthHelper(Context context){
        this.context = context;
        prefs = context.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        loadConsumerKeys();
        accessToken = loadAccessToken();
    }

    public void configureOAuth(Twitter twitter){
        twitter.setOAuthConsumer(consumerKey,consumerSecretKey);

        twitter.setOAuthAccessToken(accessToken);
    }

    public boolean hasAccessToken(){
        return (accessToken!=null);
    }

    /***
     * Loads the consumer and consumer secret key from the oauth.properties file
     */
    private void loadConsumerKeys(){
        try{
            Properties props = new Properties();
            InputStream stream = context.getResources().openRawResource(R.raw.oauth);
            props.load(stream);
            consumerKey = (String)props.get("consumer_key");
            consumerSecretKey = props.getProperty("consumer_secret_key");

        } catch (IOException e){
            throw new RuntimeException("Unable to load consumer key from oauth.properties file", e); //TODO: incredibly shitty way of handling this
        }
    }

    /***
     * Stores the authorization key (token) and auth secret key (token secret) into the SharedPrefs object.
     * @param accessToken The actual AccessToken object where both keys are taken from.
     */
    public void storeAccessToken(AccessToken accessToken) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(AUTH_KEY, accessToken.getToken());
        editor.putString(AUTH_SECRET_KEY, accessToken.getTokenSecret());
        editor.commit();
        this.accessToken = accessToken;

    }

    /***
     * Loads the previously saved token and token secret keys and generates a new AccessToken object.
     * @return a new Access Token based on AUTH_KEY and AUT_SECRET_KEY, null if no value is found.
     */
    public AccessToken loadAccessToken(){
        String token = prefs.getString(AUTH_KEY,null);
        String tokenSecret = prefs.getString(AUTH_SECRET_KEY,null);
        if (token!=null && tokenSecret!=null){
            return new AccessToken(token,tokenSecret);
        }else{
            return null;
        }
    }
}
