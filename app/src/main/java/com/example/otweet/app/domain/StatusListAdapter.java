package com.example.otweet.app.domain;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.example.otweet.app.R;
import com.example.otweet.app.activities.StatusListItem;
import java.util.ArrayList;
import twitter4j.ResponseList;
import twitter4j.Status;

public class StatusListAdapter extends ArrayAdapter<Status> {

    private Context context;

    public StatusListAdapter(Context context, ResponseList statuses){
        super(context,android.R.layout.simple_list_item_1, statuses);
        this.context = context;
    }

    public long getFirstId(){
        Status firstStatus = getItem(0);
        if (firstStatus == null){
            return 0;
        } else{
            return firstStatus.getId();
        }

    }

    public long getLastId(){
        Status lastStatus = getItem(getCount()-1); //NPE
        if (lastStatus==null) return 0;
        else return lastStatus.getId();
    }

    public void appendNewer(ResponseList<Status> statusList){
        setNotifyOnChange(false);
        for (Status s : statusList){
            insert(s, 0);
        }
        notifyDataSetChanged();
    }

    public void appendOlder(ResponseList<Status> statusList){
        setNotifyOnChange(false);
        for (Status s: statusList){
            insert(s,getCount()); //npe add -1
        }
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        StatusListItem v;
        if (convertView == null){
            v = (StatusListItem)View.inflate(context, R.layout.status_list_item, null);
        }else{
            v = (StatusListItem)convertView;
        }
        v.setStatus((Status)getItem(position)); //FIXME: redundant cast
        return v;
    }


}
