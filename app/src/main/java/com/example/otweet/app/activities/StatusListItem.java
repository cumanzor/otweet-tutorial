package com.example.otweet.app.activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.otweet.app.R;

import twitter4j.Status;
import twitter4j.User;

public class StatusListItem extends RelativeLayout {
    private TextView screenName;
    private TextView statusText;
    protected Drawable avatarDrawable;

    public StatusListItem(Context context, AttributeSet attrs){
        super(context,attrs);
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();
        findViews();
    }

    public void setStatus(Status status){
        final User user = status.getUser();
        screenName.setText(user.getScreenName());
        statusText.setText(status.getText());
    }

    private void findViews() {
        screenName = (TextView)findViewById(R.id.status_user_name_text);
        statusText = (TextView)findViewById(R.id.status_text);
    }


}
